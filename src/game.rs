
extern crate regex;

use std::io::{stdout, stdin, Write};
use regex::Regex;
use std::str::FromStr;

mod field;
mod ai;


fn get_user_mode_input() -> Option<u32> {
    let mut s = String::new();
    let _ = stdout().flush();
    stdin().read_line(&mut s).expect("Did not enter a correct string");

    let re = Regex::new(r"^\s*(\d+)\s*").unwrap();

    match re.captures(&s) {
        Some(cap) => Some(u32::from_str(&cap[1]).unwrap()),
        None => None
    }
}


fn get_user_turn_input() -> Option<(u32, field::Direction)> {
    let mut s = String::new();
    let _ = stdout().flush();
    stdin().read_line(&mut s).expect("Did not enter a correct string");

    let re = Regex::new(r"^\s*(\d+)\s*((?i)[T,B,L,R](?-i))\s*").unwrap();

    if let Some(cap) = re.captures(&s) {
        // Check if both coordinates are in the field
        let box_id = u32::from_str(&cap[1]).unwrap();
        let direction = match &cap[2] {
            "t" => field::Direction::Top,
            "T" => field::Direction::Top,
            "b" => field::Direction::Bottom,
            "B" => field::Direction::Bottom,
            "l" => field::Direction::Left,
            "L" => field::Direction::Left,
            "r" => field::Direction::Right,
            "R" => field::Direction::Right,
            c => panic!("Got an unexpected char: {}", c)
        };
        return Some((box_id, direction));
    }
    None
}


fn init_game() -> (field::Field, bool) {
    println!(
        "Servus tomato! Which mode would you like to play?\n\
        1: smash your best friend\n\
        2: kill the computer\n"
    );

    let ai_flag: bool;
    loop {
        if let Some(input) = get_user_mode_input() {
            match input {
                1 => { ai_flag = false; break; },
                2 => { ai_flag = true; break; },
                _ => ()
            }
        }
        println!("Same player, try again!");
    }

    println!("How big should your field be? Enter width and height seperatly.");

    loop {
        if let Some(width) = get_user_mode_input() {
            if let Some(height) = get_user_mode_input() {
                return (field::Field::new(width, height), ai_flag);
            }
        }
        println!("Same player, try again!");
    }
}


pub fn game() {
    let (mut field, ai_flag) = init_game();
    let mut current_player = field::Player::new();
    let ai_player = field::Player::Black;

    loop {
        if ai_flag && current_player == ai_player {

            match ai::make_move(&mut field, ai_player) {
                ai::Response::Nothing => println!("The AI made its turn."),
                ai::Response::Scored => {
                    println!("The AI scored a point. Scared, Potter?");
                    field.print_player_count();
                },
                ai::Response::Resign => {
                    println!("The AI gave up. Your skill is over 9000!\n{}", field);
                    field.print_player_count();
                    break;
                }
            }

            current_player.change();

        } else {
            println!(
                "Player {:?}, it is your turn!\n\
                Choose a box and a direction to draw\n\
                {}\n\
                Input: ", current_player, field
            );

            loop {
                if let Some((box_id, direction)) = get_user_turn_input() {
                    if let Some(point_flag) = field.draw_line(box_id, direction, current_player) {
                        if point_flag {
                            field.print_player_count();
                            println!("Hell yeah, you scored a point player {:?}.\n\
                                      You can do it again!", current_player);
                        } else {
                            current_player.change();
                        }
                        break;
                    }
                }
                println!("Same player, try again!")
            }

            if let Some(winner) = field.get_winner() {
                match winner {
                    Some(winner) => {
                        if ai_flag && winner == ai_player {
                            println!("The AI fucked you... Sorry player {:?}!", winner.clone().change());
                        } else {
                            println!("You broke the game. Congratz player {:?}!", winner)
                        }
                    },
                    None => println!("Unbelievable, both of you lost the game. Go shame yourself.")
                }

                field.print_player_count();
                break;
            }
        }
    }
}