
extern crate rand;

use rand::Rng;

use super::field;


pub enum Response {
    Nothing,
    Scored,
    Resign
}

fn random_move(field: &field::Field) -> (u32, field::Direction) {
    loop {
        let box_id = rand::thread_rng().gen_range(0, field.n_boxes());
        let direction = match rand::thread_rng().gen_range(0, field::Direction::DirectionCount as u8) {
            0 => field::Direction::Top,
            1 => field::Direction::Bottom,
            2 => field::Direction::Left,
            3 => field::Direction::Right,
            _ => panic!("This should not happen.")
        };
        if field.is_valid_move(box_id, direction) {
            return (box_id, direction);
        }
        println!("The AI is drunk. It tried to draw an already existing line.");
    }
}

fn make_random_move(field: &mut field::Field, player: field::Player) -> Response {
    let (box_id, direction) = random_move(field);
    match field.draw_line(box_id, direction, player) {
        Some(true) => Response::Scored,
        Some(false) => Response::Nothing,
        None => panic!("This should not happen.")
    }
}

fn make_improved_random_move(field: &mut field::Field, player: field::Player) -> Response {
    let mut response = Response::Nothing;
    if field.take_free_points(player) {
        response = Response::Scored;
    }

    for _ in 0..field.n_boxes() {
        let (box_id, direction) = random_move(field);
        if field.is_ok_move(box_id) {
            field.draw_line(box_id, direction, player);
            return response;
        }
    }
    // Fallback if the KI can not find a random ok move
    make_random_move(field, player);
    response
}

pub fn make_move(field: &mut field::Field, player: field::Player) -> Response {
    make_improved_random_move(field, player)
}