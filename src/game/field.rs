
use std::fmt;


#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
pub enum Player {
    White,
    Black,
    PlayerCount
}

impl Player {
    pub fn new() -> Self {
        Player::White
    }

    pub fn change(&mut self) {
        *self = match self {
            Player::White => Player::Black,
            Player::Black => Player::White,
            Player::PlayerCount => panic!("You can not change the player count.")
        }
    }
}


#[derive(PartialEq, Eq, Hash, Debug)]
struct Points {
    points: [u32; Player::PlayerCount as usize]
}

impl Points {
    fn new() -> Self {
        Self { points: [0; Player::PlayerCount as usize] }
    }

    fn of(&self, player: Player) -> u32 {
        self.points[player as usize]
    }

    fn of_mut(&mut self, player: Player) -> &mut u32 {
        &mut self.points[player as usize]
    }

    fn add_for(&mut self, player: Player) {
        *self.of_mut(player) += 1;
    }

    fn get_winner(&self) -> Option<Player> {
        if self.points[0] > self.points[1] { Some(Player::White) }
        else if self.points[1] > self.points[0] { Some(Player::Black) }
        else { None }
    }
}


#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
pub enum Direction {
    Top,
    Bottom,
    Left,
    Right,
    DirectionCount
}


impl Direction {
    pub fn get(i: usize) -> Option<Self> {
        match i {
            0 => Some(Direction::Top),
            1 => Some(Direction::Bottom),
            2 => Some(Direction::Left),
            3 => Some(Direction::Right),
            _ => None
        }
    }

    pub fn opposite(&self) -> Self {
        match self {
            Direction::Top => Direction::Bottom,
            Direction::Bottom => Direction::Top,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
            Direction::DirectionCount => panic!("You can not change the direction count.")
        }
    }
}


#[derive(PartialEq, Eq, Hash, Debug)]
struct Box {
    // TODO use BitVec instead?
    borders: [bool; Direction::DirectionCount as usize]
}

impl Box {
    fn new() -> Self {
        Self { borders: [false; Direction::DirectionCount as usize] }
    }

    fn border(&self, direction: Direction) -> bool {
        self.borders[direction as usize]
    }

    fn border_mut(&mut self, direction: Direction) -> &mut bool {
        &mut self.borders[direction as usize]
    }

    fn is_set(&self, direction: Direction) -> bool {
        self.borders[direction as usize]
    }

    fn set_border(&mut self, direction: Direction) -> bool {
        if !self.border(direction) {
            *self.border_mut(direction) = true;
            return true;
        }
        false
    }

    fn get_value(&self) -> u32 {
        let mut sum = 0;
        for b in self.borders.iter() {
            sum += *b as u32;
        }
        sum
    }

    fn get_first_false(&self) -> Option<Direction> {
        if let Some(i) = self.borders.iter().position(|&x| !x) {
            Direction::get(i)
        } else { None }
    }
}


#[derive(PartialEq, Eq, Hash, Debug)]
pub struct Field {
    width: u32,
    height: u32,
    remining_boxes: u32,
    // The order of the boxes is from left to right and top to bottom, starting at 0.
    // Each box holds bool variables which indicate, whether the box in bordered to a specific side.
    // Keep in mind that the borders of two adjacent boxes overlap.
    boxes: Vec<Box>,
    points: Points,
    visualization: String // TODO do we need to save the current visualization?
    // should not be saved in the same struct the AI will use
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.visualization)
    }
}

impl Field {
    // Without the outer borders
    //  _ _ _
    // | . . |
    // | . . |
    // |_ _ _|
    //
    fn new_visualization(width: u32, height: u32) -> String {
        // TODO add border naming for row and column
        // or name cells directly

        let new_width = (2 * width + 1) as usize;
        let max_leading_spaces = (height * (width - 1)).to_string().len() as usize;
        let mut vis = format!("{}  ", " ".repeat(max_leading_spaces));

        // x index
        for i in 0..width as usize {
            vis += &format!("{} ", i);
        }
        vis += "\n";

        // first row
        vis += &format!("{}0 {}\n", " ".repeat(max_leading_spaces - 1), " ".repeat(new_width));

        for i in 1..height - 1 {
            let current_start_box = width * i;
            vis += &format!("{}{}  {}  \n",
                            " ".repeat(max_leading_spaces - current_start_box.to_string().len()),
                            current_start_box,
                            " .".repeat((width - 1) as usize));
        }

        // last row
        vis += &format!("{} {}\n", height * (width - 1), " ".repeat(new_width));

        vis
    }

    fn update_visualization(&mut self, box_id: u32, direction: Direction) {
        assert!(box_id < self.width * self.height);

        // Calculate the position in string which shall be replaced.
        // Remember to count '\n'.
        let height_digit_length = self.height.to_string().len() as u32 + 1;
        let offset_row = 2 * self.width + 2 + height_digit_length;
        let offset_y = offset_row * (box_id / self.height + 1);
        let offset_x = 2 * (box_id % self.width + 1) + height_digit_length;
        let offset = (offset_y + offset_x) as i32;

        let (token, offset_direction) = match direction {
            Direction::Top => ("_", -1 * offset_row as i32),
            Direction::Bottom => ("_", 0),
            Direction::Left => ("|", -1),
            Direction::Right => ("|", 1),
            _ => panic!("This direction is not known: {:?}", direction)
        };

        let offset = (offset + offset_direction) as usize;

        self.visualization.replace_range((offset - 1)..offset, token);
    }

    pub fn print_player_count(&self) {
        // TODO Make player count for visualization output like
        let mut s = String::from("The new scoreboard:\n");
        for (player, pt) in
            [(Player::White, self.points.of(Player::White)),
             (Player::Black, self.points.of(Player::Black))].iter() {
            s += &format!("Player {:?}: {} pt\n", player, pt)
        }
        println!("{}", s);
    }

    pub fn new(width: u32, height: u32) -> Self {
        assert!(height > 1 && width > 1);

        let n_boxes = (width * height) as usize;

        let mut boxes: Vec<Box> = Vec::with_capacity(n_boxes);
        for _ in 0..n_boxes {
            boxes.push(Box::new());
        }

        Self {
            width,
            height,
            remining_boxes: n_boxes as u32,
            boxes,
            points: Points::new(),
            visualization: Field::new_visualization(width, height)
        }
    }

    pub fn n_boxes(&self) -> u32 {
        self.width * self.height
    }

    pub fn get_neighbor(&self, box_id: u32, direction: Direction) -> Option<u32> {
        match direction {
            Direction::Top => {
                // If the current box is in the first row, it can not have a top neighbor.
                if box_id < self.width { None } else { Some(box_id - self.width) }
            },
            Direction::Bottom => {
                // The maximum number of boxes is width*height.
                let other_id = box_id + self.width;
                if other_id <= self.width * self.height { Some(other_id) } else { None }
            },
            Direction::Left => {
                // If the current box is in the left column, it can not have a left neighbor.
                if box_id % self.width == 0 { None } else { Some(box_id - 1) }
            },
            Direction::Right => {
                // If the other box is in the left column, the current box can have no right neighbor.
                let other_id = box_id + 1;
                if other_id % self.width == 0 { None } else { Some(other_id) }
            },
            _ => panic!("This direction is not known: {:?}", direction)
        }
    }

    pub fn is_valid_move(&self, box_id: u32, direction: Direction) -> bool {
        !self.boxes[box_id as usize].is_set(direction)
    }

    // Return true if at least one point was added for the player.
    // Return false if a border could be placed, but no points were scored.
    // Return None if no border could be placed. (There was already one.)
    fn add_border(&mut self, box_id: u32, direction: Direction, player: Player) -> Option<bool> {
        if self.boxes[box_id as usize].set_border(direction) {
            if self.boxes[box_id as usize].get_value() == Direction::DirectionCount as u32 {
                self.points.add_for(player);
                self.remining_boxes -= 1;
                Some(true)
            } else {
                Some(false)
            }
        } else {
            None
        }
    }

    // Return true if at least one point was added for the player.
    // Return false if a border could be placed, but no points were scored.
    // Return None if no border could be placed. (There was already one.)
    pub fn draw_line(&mut self, box_id: u32, direction: Direction, player: Player) -> Option<bool> {
        if box_id >= self.n_boxes() { return None; }

        if let Some(point_flag) = self.add_border(box_id, direction, player) {
            self.update_visualization(box_id, direction);

            if let Some(other_id) = self.get_neighbor(box_id, direction) {
                match self.add_border(other_id, direction.opposite(), player) {
                    Some(other_point_flag) => Some(point_flag || other_point_flag),
                    None => panic!("This should not be happening.")
                }
            } else {
                Some(point_flag)
            }
        } else {
            None
        }
    }

    pub fn check_win_condition(&self) -> bool {
        return self.remining_boxes == 0
    }

    pub fn get_winner(&self) -> Option<Option<Player>> {
        if self.check_win_condition() { Some(self.points.get_winner()) }
        else { None }
    }

    //
    // Stuff for the AI
    //

    pub fn get_value(&self, box_id: u32) -> u32 {
        self.boxes[box_id as usize].get_value()
    }

    pub fn take_free_points(&mut self, player: Player) -> bool {
        let mut got_points_flag: bool = false;
        let mut repeat = true;
        while repeat {
            repeat = false;
            for box_id in 0..self.n_boxes() {
                if self.boxes[box_id as usize].get_value() == 3 {
                    if let Some(direction) = self.boxes[box_id as usize].get_first_false() {
                        assert_eq!(self.draw_line(box_id, direction, player), Some(true));
                        got_points_flag = true;
                        repeat = true;
                        break;
                    }
                }
            }
        }
        got_points_flag
    }

    pub fn is_ok_move(&self, box_id: u32) -> bool {
        self.get_value(box_id) != 2
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn player_change() {
        let mut player = Player::new();
        assert_eq!(player, Player::White);
        player.change();
        assert_eq!(player, Player::Black);
        player.change();
        assert_eq!(player, Player::White);
    }

    #[test]
    #[should_panic(expected = "You can not change the player count.")]
    fn player_change_panic_wrong_player_count() {
        let mut player = Player::PlayerCount;
        player.change();
    }

    #[test]
    fn points() {
        let mut player = Player::new();
        let mut points = Points::new();
        assert_eq!(points.points, [0; Player::PlayerCount as usize]);
        assert_eq!(points.of(player), 0);
        points.add_for(player);
        assert_eq!(points.of(player), 1);
        points.add_for(player);
        assert_eq!(points.of(player), 2);
        player.change();
        assert_eq!(points.of(player), 0);
    }

    #[test]
    #[should_panic]
    fn points_get_winner() {
        unimplemented!()
    }

    #[test]
    fn direction_opposite_top_bottom() {
        let d = Direction::Top;
        assert_eq!(d, Direction::Top);
        let d = d.opposite();
        assert_eq!(d, Direction::Bottom);
        let d = d.opposite();
        assert_eq!(d, Direction::Top);
    }

    #[test]
    fn direction_opposite_left_right() {
        let d = Direction::Left;
        assert_eq!(d, Direction::Left);
        let d = d.opposite();
        assert_eq!(d, Direction::Right);
        let d = d.opposite();
        assert_eq!(d, Direction::Left);
    }

    #[test]
    #[should_panic(expected = "You can not change the direction count.")]
    fn direction_opposite_panic_wrong_direction() {
        let d = Direction::DirectionCount;
        d.opposite();
    }

    #[test]
    fn box_() {
        use self::Direction::*;

        let mut b = Box::new();
        assert_eq!(b.borders, [false, false, false, false]);
        assert_eq!(b.get_value(), 0);
        assert_eq!(b.border(Top), false);
        assert_eq!(b.set_border(Left), true);
        assert_eq!(b.borders, [false, false, true, false]);
        assert_eq!(b.get_value(), 1);
        assert_eq!(b.border(Left), true);
        assert_eq!(b.set_border(Bottom), true);
        assert_eq!(b.borders, [false, true, true, false]);
        assert_eq!(b.get_value(), 2);
        assert_eq!(b.border(Right), false);
        assert_eq!(b.set_border(Bottom), false);
        assert_eq!(b.borders, [false, true, true, false]);
        assert_eq!(b.get_value(), 2);
        assert_eq!(b.border(Bottom), true);
        assert_eq!(b.set_border(Top), true);
        assert_eq!(b.borders, [true, true, true, false]);
        assert_eq!(b.get_value(), 3);
        assert_eq!(b.set_border(Right), true);
        assert_eq!(b.borders, [true, true, true, true]);
        assert_eq!(b.get_value(), 4);
    }

    fn cmp_visulization(expected: &str, field: &Field) {
        assert_eq!(String::from(expected), field.visualization)
    }

    #[test]
    fn field_visualization() {
        /*
        let mut field = Field::new(3, 3);
        cmp_visulization("       \n  . .  \n  . .  \n       \n", &field);
        field.update_visualization(3, Direction::Left);
        cmp_visulization("       \n  . .  \n| . .  \n       \n", &field);
        field.update_visualization(3, Direction::Left);
        cmp_visulization("       \n  . .  \n| . .  \n       \n", &field);
        field.update_visualization(7, Direction::Top);
        cmp_visulization("       \n  . .  \n| ._.  \n       \n", &field);
        field.update_visualization(4, Direction::Bottom);
        cmp_visulization("       \n  . .  \n| ._.  \n       \n", &field);
        field.update_visualization(1, Direction::Bottom);
        cmp_visulization("       \n  ._.  \n| ._.  \n       \n", &field);
        field.update_visualization(3, Direction::Right);
        cmp_visulization("       \n  ._.  \n| |_.  \n       \n", &field);
        field.update_visualization(8, Direction::Bottom);
        cmp_visulization("       \n  ._.  \n| |_.  \n     _ \n", &field);
        */
    }

    #[test]
    #[should_panic(expected = "This direction is not known")]
    fn field_visualization_update_panic_wrong_direction() {
        let mut field = Field::new(4, 5);
        field.update_visualization(2, Direction::DirectionCount);
    }

    #[test]
    #[should_panic]
    fn field_visualization_update_panic_out_of_bounds() {
        let mut field = Field::new(4, 5);
        field.update_visualization(4*5+1, Direction::Top);
        cmp_visulization("x", &field);
    }

    fn cmp_neighbor(box_id: u32, direction: Direction, field: &Field, expected: Option<u32>) {
        assert_eq!(field.get_neighbor(box_id, direction), expected);
    }

    #[test]
    fn field_get_neighbor() {
        use self::Direction::*;

        let field = Field::new(5, 2);
        cmp_neighbor(0, Left, &field, None);
        cmp_neighbor(5, Left, &field, None);
        cmp_neighbor(4, Right, &field, None);
        cmp_neighbor(7, Top, &field, Some(2));
        cmp_neighbor(5, Right, &field, Some(6));
        cmp_neighbor(3, Bottom, &field, Some(8));
        cmp_neighbor(9, Left, &field, Some(8));

        let field = Field::new(4, 4);
        cmp_neighbor(10, Top, &field, Some(6));
        cmp_neighbor(9, Bottom, &field, Some(13));

    }

    #[test]
    #[should_panic]
    fn field_add_border() {
        unimplemented!()
    }

    #[test]
    #[should_panic]
    fn field_draw_line() {
        unimplemented!()
    }

    #[test]
    #[should_panic]
    fn test_check_win_condition() {
        unimplemented!()
    }

    #[test]
    #[should_panic]
    fn test_get_winner() {
        unimplemented!()
    }

}